steps = [
    [
        """
        CREATE TABLE accounts (
            id SERIAL PRIMARY KEY NOT NULL,
            username VARCHAR(255) NOT NULL UNIQUE,
            hashed_password VARCHAR(255) NOT NULL,
            email VARCHAR(255) NOT NULL
        );
        """,
        """
        DROP TABLE accounts;
        """,
    ]
]
