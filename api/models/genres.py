from pydantic import BaseModel
from typing import List


class Message(BaseModel):
    message: str


class ApiGenresIn(BaseModel):
    name: str
    api_genre_id: int


class FavObject(BaseModel):
    id: int
    api_genre_id: int
    account_id: int
    name: str


class LisGenresOut(BaseModel):
    favorite_genres: List[FavObject]


class ApiGenresOut(ApiGenresIn):
    id: int


class ApiGenresList(BaseModel):
    api_genres: List[ApiGenresOut]


class FavoriteGenresIn(BaseModel):
    api_genre_id: int


class FavoriteGenresOut(FavoriteGenresIn):
    id: int
    account_id: int
