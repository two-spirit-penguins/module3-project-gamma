from pydantic import BaseModel
from typing import List


class Message(BaseModel):
    message: str


class WatchListIn(BaseModel):
    movie_api_id: int


class WatchListOut(WatchListIn):
    id: int
    account_id: int


class WatchListArray(BaseModel):
    watch_lists: List[WatchListOut]
