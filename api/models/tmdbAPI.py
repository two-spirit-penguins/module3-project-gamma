from pydantic import BaseModel
from typing import List


class GenresListOfMovie(BaseModel):
    page: str
    results: List
