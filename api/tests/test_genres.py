from fastapi.testclient import TestClient
from authenticator import authenticator
from main import app
from queries.genres import GenresRepository


client = TestClient(app)


def fake_get_current_account_data():
    return {"id": "1", "username": "farell", "email": "farell@gmail.com"}


class FakeGenresRepository:
    def get_all_api_genre_id(self):
        return {
            "api_genres": [{"name": "Comedy", "api_genre_id": 35, "id": 1}]
        }

    def get_list_favorite_genres_by_id(self, account_id: int):
        return {
            "favorite_genres": [
                {
                    "id": 2,
                    "api_genre_id": 35,
                    "account_id": 1,
                    "name": "Comedy",
                }
            ]
        }

    def delete_favorite_genres(self, favorite_genres_id, account_id):
        return True


def test_get_all_api_genre_id():
    app.dependency_overrides[GenresRepository] = FakeGenresRepository
    res = client.get("/api/genres")
    data = res.json()
    assert res.status_code == 200
    assert data == {
        "api_genres": [{"name": "Comedy", "api_genre_id": 35, "id": 1}]
    }


def test_create_list_favorite_genres():
    app.dependency_overrides[GenresRepository] = FakeGenresRepository
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    res = client.get("/api/favorite_genres/mine")
    data = res.json()
    assert res.status_code == 200
    assert data == {
        "favorite_genres": [
            {
                "id": 2,
                "api_genre_id": 35,
                "account_id": 1,
                "name": "Comedy",
            }
        ]
    }


def test_delete_favorite_genres():
    app.dependency_overrides[GenresRepository] = FakeGenresRepository
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    res = client.delete("/api/favorite_genres/2")
    data = res.json()
    assert res.status_code == 200
    assert data == True
