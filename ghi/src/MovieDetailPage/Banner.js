import {
  useGetMovieById_alteredQuery,
  useGetCastByApiIdQuery,
} from "../store/ApiMovies";
import requests from "../request";
import "./Banner.css";
function Banner(props) {
  const { movie_api_id } = props;
  const { data: content } = useGetMovieById_alteredQuery(movie_api_id);
  const { data: casts } = useGetCastByApiIdQuery(movie_api_id);

  return (
    <>
      <div className="mv-banner">
        <header
          className="mv-banner__header"
          style={{
            background: "cover",
            backgroundImage: `url(${content?.backdrop})`,
            backgroundPosition: " center center",
            backgroundRepeat: "no-repeat",
          }}
        ></header>
        <div className="mv-banner--fadebottom "></div>
        <div className="mv-container">
          <div className="mv-container__flex">
            <div className="mv-container__image">
              <img
                className="mv-image__cotainer__card"
                src={content?.poster}
                alt=""
              ></img>
            </div>
            <div className="mv-container__movieinfo">
              <div className="mv-container__movieinfo__name">
                <h1 className="mv-title">{content?.title}</h1>
              </div>
              <div className="mv-container__movieinfo__genres">
                {content?.genres.map((genre) => {
                  return (
                    <span key={genre.id} className="mv-genre">
                      {genre.name}
                    </span>
                  );
                })}
              </div>
              <div className="mv-container__movieinfo__synopsis">
                <p>{content?.overview}</p>
              </div>
              <div className="mv-container__movieinfo__cast">
                {casts?.map((cast) => {
                  if (cast.profile_path) {
                    return (
                      <div key={cast.id}>
                        <div>
                          {
                            <img
                              key={cast.id}
                              className="mv-cast__picture"
                              src={`${requests.baseImageUrl}${cast.profile_path}`}
                              alt=""
                            ></img>
                          }
                          <div className="mv-cast__name__container">
                            <p className="mv-cast__name">{cast.name}</p>
                          </div>
                        </div>
                      </div>
                    );
                  }
                })}
              </div>

              <div className=""></div>
              <div className=""></div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
export default Banner;
