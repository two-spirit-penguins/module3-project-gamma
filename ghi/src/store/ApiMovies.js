import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const favMovieApi = createApi({
  reducerPath: "favoriteMovies",
  baseQuery: fetchBaseQuery({ baseUrl: process.env.REACT_APP_API_HOST }),
  endpoints: (builder) => ({
    getFavMovie: builder.query({
      query: () => ({
        url: `api/favorite_genres/mine`,
        credentials: "include",
      }),
      providesTags: ["favoriteGenres"],
    }),
    getAllGenres: builder.query({
      query: (allGenres) => ({
        url: `api/genres`,
        credentials: "include",
        method: "GET",
      }),
    }),
    // favorites genres
    favoriteGenresById: builder.mutation({
      query: () => ({
        url: "/api/favorite_genres/mine",
        credentials: "include",
        method: "GET",
      }),
      providesTags: ["favoriteGenres"],
    }),
    createFavoriteGenres: builder.mutation({
      query: (favoriteGenres) => ({
        url: "/api/favorite_genres",
        method: "POST",
        credentials: "include",
        body: favoriteGenres,
      }),
      invalidatesTags: ["favoriteGenres"],
    }),
    deleteFavoriteGenres: builder.mutation({
      query: (favoriteGenresId) => ({
        url: `/api/favorite_genres/${favoriteGenresId}`,
        credentials: "include",
        method: "DELETE",
      }),
      invalidatesTags: ["favoriteGenres"],
    }),

    // accounts
    getToken: builder.query({
      query: () => ({
        url: `/token`,
        credentials: "include",
      }),
      transformResponse: (response) => response?.account || undefined,
      providesTags: ["account"],
      invalidatesTags: ["token"],
    }),
    logout: builder.mutation({
      query: () => ({
        url: "/token",
        method: "DELETE",
        credentials: "include",
      }),
      invalidatesTags: ["account"],
    }),
    login: builder.mutation({
      query: (info) => {
        let formData = null;
        if (info instanceof HTMLElement) {
          formData = new FormData(info);
        } else {
          formData = new FormData();
          formData.append("username", info.username);
          formData.append("password", info.password);
        }
        return {
          url: "/token",
          method: "POST",
          body: formData,
          credentials: "include",
        };
      },
      invalidatesTags: ["account"],
    }),
    signup: builder.mutation({
      query: (data) => ({
        url: "/api/accounts",
        method: "POST",
        body: data,
        credentials: "include",
      }),
      invalidatesTags: ["account"],
    }),
    updatePassword: builder.mutation({
      query: (data) => ({
        url: "/api/accounts/password",
        method: "PATCH",
        body: data,
        credentials: "include",
      }),
      invalidatesTags: ["account"],
    }),
    updateEmail: builder.mutation({
      query: (data) => ({
        url: "/api/accounts/email",
        method: "PATCH",
        body: data,
        credentials: "include",
      }),
      invalidatesTags: ["account"],
    }),
    updateUsername: builder.mutation({
      query: (data) => ({
        url: "/api/accounts/username",
        method: "PATCH",
        body: data,
        credentials: "include",
      }),
      invalidatesTags: ["account"],
    }),
    // watch list s
    getWatchList: builder.query({
      query: () => ({
        url: "/api/watchlists/mine",
        credentials: "include",
      }),
      providesTags: ["watchlist", "token"],
    }),
    getWatchListOnlyId: builder.query({
      query: () => ({
        url: "/api/watchlists/mine/onlyApiid",
        credentials: "include",
      }),
      providesTags: ["watchlist", "token"],
    }),
    createWatchlist: builder.mutation({
      query: (movie_api_id) => ({
        url: "/api/watchlists",
        credentials: "include",
        method: "POST",
        body: movie_api_id,
      }),
      invalidatesTags: ["watchlist"],
    }),

    deleteWatchlist: builder.mutation({
      query: (watchlistItemID) => ({
        url: `/api/watchlists/${watchlistItemID}`,
        credentials: "include",
        method: "DELETE",
      }),
      invalidatesTags: ["watchlist"],
    }),
    // comments
    getCommentsByAccountId: builder.query({
      query: () => ({
        url: `/api/comments/mine`,
        credentials: "include",
      }),
    }),
    getCommentsByMovieApiId: builder.query({
      query: (movieApiId) => ({
        url: `/api/movies/${movieApiId}/comments`,
        // credentials: "include",
      }),
      providesTags: ["comments"],
    }),
    createComment: builder.mutation({
      query: (newComment) => ({
        url: "/api/comments",
        method: "POST",
        body: newComment,
        credentials: "include",
      }),
      invalidatesTags: ["comments"],
    }),
    updateComment: builder.mutation({
      query: ({ comment_id, ...updated_comment }) => ({
        url: `/api/comments/${comment_id}`,
        method: "PATCH",
        body: updated_comment,
        credentials: "include",
      }),
      invalidatesTags: ["comments"],
    }),
    deleteComment: builder.mutation({
      query: (data) => ({
        url: `/api/comments/${data.value}`,
        method: "DELETE",
        credentials: "include",
      }),
      invalidatesTags: ["comments"],
    }),
    // tmdbAPI
    getMoviesByGenresId: builder.query({
      query: (genre_api_id) => ({
        url: `/api/TmdbApi/sort_by_genresId/${genre_api_id}`,
      }),
      transformResponse: (response) => (response ? response : undefined),
    }),
    getTopRatedMovies: builder.query({
      query: () => ({
        url: `/api/TmdbApi/top_rated`,
      }),
      transformResponse: (response) => (response ? response : undefined),
    }),
    getPopularMovies: builder.query({
      query: () => ({
        url: `/api/TmdbApi/popular`,
      }),
      transformResponse: (response) => (response ? response : undefined),
    }),
    getUpcomingMovies: builder.query({
      query: () => ({
        url: `/api/TmdbApi/upcoming`,
      }),
      transformResponse: (response) => (response ? response : undefined),
    }),
    getMovieById_altered: builder.query({
      query: (id) => ({
        url: `api/TmdbApi/sort_by_Api_id/${id}`,
      }),
      transformResponse: (response) => (response ? response : undefined),
    }),
    getCastByApiId: builder.query({
      query: (id) => ({
        url: `/api/TmdbApi/credits/${id}`,
      }),
      transformResponse: (response) => (response ? response : undefined),
    }),
    getVideoByApiId: builder.query({
      query: (id) => ({
        url: `api/TmdbApi/videos/${id}`,
      }),
      transformResponse: (response) => {
        return response[0].key;
      },
    }),
    getBackdropsByApiId: builder.query({
      query: (id) => ({
        url: `/api/TmdbApi/backdrops/${id}`,
      }),
    }),
    getMovieBykeyWords: builder.query({
      query: (keywords) => ({
        url: `/api/TmdbApi/keywords/${keywords}`,
      }),
      transformResponse: (response) => {
        return response["results"];
      },
    }),
  }),
});

export const {
  // accounts
  useLogoutMutation,
  useLoginMutation,
  useSignupMutation,
  useUpdatePasswordMutation,
  useUpdateEmailMutation,
  useUpdateUsernameMutation,
  useGetTokenQuery,

  // genres
  useGetFavMovieQuery,
  useGetAllGenresQuery,
  useCreateFavoriteGenresMutation,
  useDeleteFavoriteGenresMutation,
  useFavoriteGenresByIdMutation,

  //watchlists
  useGetWatchListQuery,
  useGetWatchListOnlyIdQuery,
  useCreateWatchlistMutation,
  useDeleteWatchlistMutation,

  //comments
  useGetCommentsByMovieApiIdQuery,
  useGetCommentsByAccountIdQuery,
  useCreateCommentMutation,
  useUpdateCommentMutation,
  useDeleteCommentMutation,

  //tmdbAPI
  useGetMoviesByGenresIdQuery,
  useGetTopRatedMoviesQuery,
  useGetPopularMoviesQuery,
  useGetUpcomingMoviesQuery,
  useGetMovieById_alteredQuery,
  useGetCastByApiIdQuery,
  useGetBackdropsByApiIdQuery,
  useGetVideoByApiIdQuery,
  useGetMovieBykeyWordsQuery,
} = favMovieApi;
