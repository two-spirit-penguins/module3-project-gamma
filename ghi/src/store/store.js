import { configureStore} from "@reduxjs/toolkit";
import { favMovieApi } from "./ApiMovies";
import { setupListeners } from "@reduxjs/toolkit/dist/query";

export const store = configureStore({
  reducer: {
    [favMovieApi.reducerPath]: favMovieApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(favMovieApi.middleware),
});

setupListeners(store.dispatch);
